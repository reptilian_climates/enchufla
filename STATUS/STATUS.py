

def FIND_BINARIES_PATH ():
	import pathlib
	FIELD = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	STARTABLE = normpath (join (
		FIELD, 
		"../STRUCTURE/MODULES/SCRIPTS"
	))

	return STARTABLE


def ADD_PATHS ():
	import sys
	import os

	PATH = os.getenv('PATH');	
	os.environ ['PATH'] = FIND_BINARIES_PATH () + ":" + PATH

	return;

'''
	(env PYTHONPATH=MODULES_PIP python3 MODULES_PIP/bin/ENCHUFLA)
'''
def ADD_PATHS_TO_SYSTEM (PATHS):
	import pathlib
	FIELD = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	import sys
	for PATH in PATHS:
		sys.path.insert (0, normpath (join (FIELD, PATH)))

ADD_PATHS_TO_SYSTEM ([
	'STRUCTURES_PIP'
])
ADD_PATHS ()



import BODY_SCAN

import pathlib
THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

from os.path import dirname, join, normpath
STRUCTURE = normpath (join (THIS_FOLDER, "../STRUCTURE"))
OATHS = normpath (join (THIS_FOLDER, "OATHS"))

SCAN = BODY_SCAN.START (
	# REQUIRED
	#GLOB = OATHS + '/**/OATH*.py',
	GLOB = OATHS + '/**/OATH_4.py',
	
	SIMULTANEOUS = True,
	
	# OPTIONAL
	MODULE_PATHS = [
		normpath (join (OATHS, "MODULES")),
	
		normpath (join (STRUCTURE, "MODULES")),
		normpath (join (STRUCTURE, "MODULES_PIP"))
	],
	
	# OPTIONAL
	RELATIVE_PATH = OATHS
)



#
#
#