
import pathlib
from os.path import dirname, join, normpath
	
def FIND_PATH (LABEL):
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()
	MESSAGE_PATH = normpath (join (THIS_FOLDER, "STRATEGIES", LABEL))
	
	return MESSAGE_PATH
	

