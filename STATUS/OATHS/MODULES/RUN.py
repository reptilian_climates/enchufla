


	
def RUN (STRING):
	import os
	PYTHONPATH = os.environ.get ('PYTHONPATH');
	#print ("PYTHONPATH:", PYTHONPATH)

	import sys
	#print ("SYSPATH", sys.path)
	
	ENV = os.environ
	ENV ['PYTHONPATH'] = ":".join (sys.path)
	
	import subprocess
	subprocess.run (
		STRING, 
		shell = True, 
		check = True,
		cwd = FIND_CWD (),
		env = ENV
	)

	return;