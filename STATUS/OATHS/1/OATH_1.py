
'''
	
'''

import time
from BOTANIST.PROCESSES.START_MULTIPLE import START_MULTIPLE

def FIND_CWD ():
	import pathlib
	from os.path import dirname, join, normpath
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	return normpath (join (THIS_FOLDER, "LAB"))

def RUN (STRING):
	import os
	PYTHONPATH = os.environ.get ('PYTHONPATH');
	#print ("PYTHONPATH:", PYTHONPATH)

	import sys
	#print ("SYSPATH", sys.path)
	
	ENV = os.environ
	ENV ['PYTHONPATH'] = ":".join (sys.path)
	
	import subprocess
	subprocess.run (
		STRING, 
		shell = True, 
		check = True,
		cwd = FIND_CWD (),
		env = ENV
	)

	return;

def CLEAN (
	PRIVATE_KEY,
	PUBLIC_KEY,
	SIGNED_MESSAGE_PATH
):
	try:
		RUN (
			" ".join ([
				'rm ' + PRIVATE_KEY
			])
		)
	except Exception as E:
		pass;
		
	try:
		RUN (
			" ".join ([
				'rm ' + PUBLIC_KEY
			])
		)
	except Exception as E:
		pass;

	try:
		RUN (
			" ".join ([
				'rm ' + SIGNED_MESSAGE_PATH
			])
		)
	except Exception as E:
		pass;
		
	try:
		RUN (
			" ".join ([
				'rm ' + UNSIGNED_MESSAGE_PATH
			])
		)
	except Exception as E:
		pass;

	return;

import MESSAGES

def CHECK_1 ():
	print ("CHECK 1")
	
	PRIVATE_KEY = "ED448_PRIVATE_KEY.DER"
	PUBLIC_KEY = "ED448_PUBLIC_KEY.DER"
	
	UNSIGNED_MESSAGE_PATH = MESSAGES.FIND_PATH ("1.TXT")
	SIGNED_MESSAGE_PATH = "SIGNED_MESSAGE.BYTES"
	
	print ("UNSIGNED_MESSAGE_PATH:", UNSIGNED_MESSAGE_PATH)
	
	#return;
	
	
	CLEAN (
		PRIVATE_KEY,
		PUBLIC_KEY,
		SIGNED_MESSAGE_PATH
	)
	
	RUN (
		" ".join ([
			'ENCHUFLA DUOM ED448 BUILD-PRIVATE-KEY',
			f'--path "{ PRIVATE_KEY }"',
			'--seed "5986888b11358bf3d541b41eea5daece1c6eff64130a45fc8b9ca48f3e0e02463c99c5aedc8a847686d669b7d547c18fe448fc5111ca88f4e8"'
		])
	)
	
	RUN (
		" ".join ([
			'ENCHUFLA DUOM ED448 BUILD-PUBLIC-KEY',
			f'--private-key-path "{ PRIVATE_KEY }"',
			f'--public-key-path "{ PUBLIC_KEY }"'
		])
	)
	print ("& PRIVATE KEY BUILT")
	print ()
	
	RUN (
		" ".join ([
			'ENCHUFLA DUOM ED448 BUILD-PUBLIC-KEY',
			f'--private-key-path "{ PRIVATE_KEY }"',
			f'--public-key-path "{ PUBLIC_KEY }"'
		])
	)
	print ("& PUBLIC KEY BUILT")
	print ()
	
	RUN (
		" ".join ([
			'ENCHUFLA DUOM ED448 SIGN',
			f'--private-key-path "{ PRIVATE_KEY }"',
			f'--unsigned-utf8-path "{ UNSIGNED_MESSAGE_PATH }"',
			f'--signed-bytes-path "{ SIGNED_MESSAGE_PATH }"'
		])
	)
	print ("& MESSAGE SIGNED")
	print ()
	
	RUN (
		" ".join ([
			'ENCHUFLA DUOM ED448 VERIFY',
			f'--public-key-path "{ PUBLIC_KEY }"',
			f'--unsigned-utf8-path "{ UNSIGNED_MESSAGE_PATH }"',
			f'--signed-bytes-path "{ SIGNED_MESSAGE_PATH }"'
		])
	)
	print ("& MESSAGE VERIFIED")
	print ()
	
	CLEAN (
		PRIVATE_KEY,
		PUBLIC_KEY,
		SIGNED_MESSAGE_PATH
	)


	return;
	
	
CHECKS = {
	"ED448 SEQUENCE": CHECK_1
}