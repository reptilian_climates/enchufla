
'''
	
'''

import time
from BOTANIST.PROCESSES.START_MULTIPLE import START_MULTIPLE

def FIND_CWD ():
	import pathlib
	from os.path import dirname, join, normpath
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	return normpath (join (THIS_FOLDER, "LAB"))

def FIND_PATH_FROM_THIS_FOLDER (PATH):
	import pathlib
	from os.path import dirname, join, normpath
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	return normpath (join (THIS_FOLDER, PATH))


def FIND_ENCHUFLA_PATH ():
	import pathlib
	from os.path import dirname, join, normpath
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	return normpath (join (THIS_FOLDER, "../../../STRUCTURE/MODULES/SCRIPTS/ENCHUFLA"))

def RUN (STRING):
	import os
	PYTHONPATH = os.environ.get ('PYTHONPATH');
	#print ("PYTHONPATH:", PYTHONPATH)

	import sys
	#print ("SYSPATH", sys.path)
	
	ENV = os.environ
	ENV ['PYTHONPATH'] = ":".join (sys.path)
	
	import subprocess
	subprocess.run (
		STRING, 
		shell = True, 
		check = True,
		cwd = FIND_CWD (),
		env = ENV
	)

	return;



import MESSAGES
import ENCHUFLA

def CHECK_1 ():
	print ("OATH 2, CHECK 1")
	
	KEG = FIND_ENCHUFLA_PATH () + " KEG TAP"
	
	from BOTANIST.PROCESSES.START_MULTIPLE import START_MULTIPLE
	PROCS = START_MULTIPLE (
		PROCESSES = [
			{ 
				"STRING": KEG,
				"CWD": None
			}
		]
	)


	import time
	time.sleep (.5)
	
	PROCS ["EXIT"] ()
	
	return;


	
	
	
CHECKS = {
	"CAN START AND STOP THE KEG": CHECK_1
}