
'''
	
'''

import time
from BOTANIST.PROCESSES.START_MULTIPLE import START_MULTIPLE

def FIND_CWD ():
	import pathlib
	from os.path import dirname, join, normpath
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	return normpath (join (THIS_FOLDER, "FIRM"))

def FIND_PATH_FROM_THIS_FOLDER (PATH):
	import pathlib
	from os.path import dirname, join, normpath
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	return normpath (join (THIS_FOLDER, PATH))


def FIND_ENCHUFLA_PATH ():
	import pathlib
	from os.path import dirname, join, normpath
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	return normpath (join (THIS_FOLDER, "../../../STRUCTURE/MODULES/SCRIPTS/ENCHUFLA"))

def RUN (STRING):
	import os
	PYTHONPATH = os.environ.get ('PYTHONPATH');

	import sys
	
	ENV = os.environ
	ENV ['PYTHONPATH'] = ":".join (sys.path)
	
	import subprocess
	subprocess.run (
		STRING, 
		shell = True, 
		check = True,
		cwd = FIND_CWD (),
		env = ENV
	)

	return;

def CLEAN (PATHS):
	for PATH in PATHS:
		try:
			RUN ('rm ' + PATH)
		except Exception as E:
			pass;


	return;

import MESSAGES
import ENCHUFLA

def CHECK_1 ():
	print ("OATH 3, CHECK 1")
	
	PRIVATE_KEY = "ED448_PRIVATE_KEY.DER"
	PUBLIC_KEY = "ED448_PUBLIC_KEY.DER"
	
	UNSIGNED_MESSAGE_PATH = MESSAGES.FIND_PATH ("1.TXT")
	SIGNED_MESSAGE_PATH = "SIGNED_MESSAGE.BYTES"
	
	print ("UNSIGNED_MESSAGE_PATH:", UNSIGNED_MESSAGE_PATH)
	
	CLEAN ([
		FIND_PATH_FROM_THIS_FOLDER ("FIRM/ED448_PRIVATE_KEY.DER"),
		FIND_PATH_FROM_THIS_FOLDER ("FIRM/ED448_PUBLIC_KEY.DER"),
		FIND_PATH_FROM_THIS_FOLDER ("FIRM/SIGNED.BYTES")
	])
	
	KEG = FIND_ENCHUFLA_PATH () + " KEG TAP"
	
	print ("KEG:", KEG)
	
	from BOTANIST.PROCESSES.START_MULTIPLE import START_MULTIPLE
	PROCS = START_MULTIPLE (
		PROCESSES = [
			{ 
				"STRING": KEG,
				"CWD": None
			}
		]
	)

	import time
	time.sleep (.5)

	print ()
	print ()
	print ()
	

	ENCHUFLA.PLEASE ({
		"START": "BUILD ED448 PRIVATE KEY",
		"FIELDS": {
			"SEED": "5986888b11358bf3d541b41eea5daece1c6eff64130a45fc8b9ca48f3e0e02463c99c5aedc8a847686d669b7d547c18fe448fc5111ca88f4e8",
			#"PATH": "ED448_PRIVATE_KEY.DER"
			"PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/ED448_PRIVATE_KEY.DER")
		}
	})

	print ()
	print ()
	print ()
	

	ENCHUFLA.PLEASE ({
		"START": "BUILD ED448 PUBLIC KEY",
		"FIELDS": {
			"PRIVATE KEY PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/ED448_PRIVATE_KEY.DER"),
			"PUBLIC KEY PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/ED448_PUBLIC_KEY.DER"),
			"PUBLIC KEY FORM": "DER"
		}
	})
	
	
	ENCHUFLA.PLEASE ({
		"START": "ED448 SIGN",
		"FIELDS": {
			"PRIVATE KEY PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/ED448_PRIVATE_KEY.DER"),
			"UNSIGNED BYTES PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/UNSIGNED.HTML"),
			"SIGNED BYTES PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/SIGNED.BYTES")
		}
	})


	ENCHUFLA.PLEASE ({
		"START": "ED448 VERIFY",
		"FIELDS": {
			"PUBLIC KEY PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/ED448_PUBLIC_KEY.DER"),
			"UNSIGNED BYTES PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/UNSIGNED.HTML"),
			"SIGNED BYTES PATH": FIND_PATH_FROM_THIS_FOLDER ("FIRM/SIGNED.BYTES")
		}
	})
	
	print ("VERIFIED?")
	
	time.sleep (.5)

	PROCS ["EXIT"] ()
	
	
	
CHECKS = {
	"ED448 SEQUENCE": CHECK_1
}